var canvas= document.querySelector('canvas');
canvas.width= window.innerWidth;
canvas.height= window.innerHeight;

var c= canvas.getContext('2d');

// //RECT
// c.fillStyle="red";
// c.fillRect(100,200,100,100);

// //LINES

// c.beginPath();
// c.moveTo(300,300);
// c.lineTo(400,400);
// c.lineTo(500,400);
// c.lineTo(600,300);
// c.strokeStyle="blue";
// c.stroke();

// //arcs

// c.beginPath();
// c.arc(500,100,50,0,1.5*Math.PI);
// c.stroke();

// //for loop

// for(var i=0;i<100;i++){
// 	var x=Math.random()*window.innerWidth;
// var y=Math.random()*window.innerHeight;
// 	c.beginPath();
// c.arc(x,y,50,0,2*Math.PI);
// c.stroke();
// }

//animation
var mouse={
	x:undefined,
	y:undefined
}
var maxRadius=40;


var colorArray=['#C004D9','#AB05F2','#6D0FF2','#3316F2','#0D0D0D'];
window.addEventListener('mousemove',function(event){
	
	mouse.x=event.x;
	mouse.y=event.y;
	console.log(mouse);
})
window.addEventListener('resize',function(){
	canvas.width=window.innerWidth;
	canvas.height=window.innerHeight;
	init();
})

function Circle(x,y,radius,dx,dy){
	this.x=x;
	this.y=y;
	this.radius=radius;
	this.minRadius=radius;
	this.dx=dx;
	this.dy=dy;
	this.circleColor=colorArray[Math.floor(Math.random()* colorArray.length)];
	this.draw = function(){
	c.beginPath();
	c.arc(this.x,this.y,this.radius,0,2*Math.PI);
	
	
	
	
	c.fillStyle=this.circleColor;
	c.fill();
	
	}
	this.update= function(){
		if(this.x+this.radius>=innerWidth || this.x-this.radius <0){
		this.dx=-this.dx;
	}
	if(this.y+this.radius>=innerHeight || this.y-this.radius <0){
		this.dy=-this.dy;
	}

	this.x+= this.dx;
	this.y+= this.dy;
	//interactivity 
	if(mouse.x - this.x<50 && mouse.x-this.x> -50
		&& mouse.y-this.y<50 &&mouse.y-this.y>-50){
		if(this.radius<maxRadius){
			this.radius+=1;
		}
		
	}else if(this.radius>this.minRadius){
		this.radius-=1;
	}
	this.draw();
	}

	
}
var circleArray=[];

function init(){
	circleArray=[];
for(var i=0;i<700;i++){
	
	var radius =Math.random()*3+1;
	var x=Math.random()*(innerWidth-radius*2)+radius ;
	var y=Math.random()*(innerHeight-radius*2)+radius;
	
	var dx = Math.random() -0.5;
	var dy = Math.random() -0.5;
	circleArray.push(new Circle(x,y,radius,dx,dy));
	}	
}




function animate(){
	requestAnimationFrame(animate);
	c.clearRect(0,0,innerWidth,innerHeight);
	for(var i=0;i<circleArray.length;i++){
		circleArray[i].update();

	}	
}
init();
animate();